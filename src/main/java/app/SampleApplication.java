package app;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.context.annotation.Import;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.SpringApplication;


import repository.AccountRepository;
import repository.InMemoryAccountRepository;
import repository.jdbc.JdbcAccountRepository;
import service.TransferService;
import domain.Account;
import config.WebConfig;
import controller.AccountController;

@Import(WebConfig.class)
//@Controller
@EnableAutoConfiguration

public class SampleApplication {
    
	@RequestMapping("/")
    @ResponseBody	
	
    String home(){
		return "hola";
	}

//    public static void main(String[] args) throws Exception {


  //      SpringApplication.run(SampleApplication.class, args);
        


    //}

	
	
	
	
	public static void main(String[] args) throws Exception {
	//	SpringApplication.run(SampleApplication.class, args);
		AccountRepository repository = new JdbcAccountRepository();
		TransferService service = new TransferService(repository);
		Account a1 = new Account("1001", 550);
		Account a2 = new Account("1002", 50);
		System.out.println(a1);
		System.out.println(a2);
		repository.save(a1);
		repository.save(a2);
		service.transfer("1001", "1002", 100);
		System.out.println(repository.findAll());
	}
}
