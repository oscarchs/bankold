package service;

import java.util.List;
import repository.AccountRepository;
import domain.Account;

public class AccountService {

	AccountRepository repository;

	public AccountService(AccountRepository repository){
		this.repository = repository;
	}

	public boolean CreateNewAccount(String number, double balance){
		if ( repository.findAccount(number) ){
			return false;
		}
		Account new_account = new Account(number,balance);
		repository.save(new_account);
		return true;
	}

	public boolean AlterBalance(String number, double new_balance){
		if ( !repository.findAccount(number) ){
			return false;
		}
		Account tmp = repository.findByNumber(number);
		tmp.setBalance(new_balance);
		return true;
	}

	public void genAccounts(){
		Account a1 = new Account("1000",100); 
		Account a2 = new Account("1001",300);
		Account a3 = new Account("1002",150);
		repository.save(a1);
		repository.save(a2);
		repository.save(a3);
	}

	public List<Account> listAccounts(){
		return repository.findAll();
	}

	public AccountRepository getRepository(){
		return repository;
	}

	public Account ShowAccount(String a){
		return repository.findByNumber(a);
	}

}
