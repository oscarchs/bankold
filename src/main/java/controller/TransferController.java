package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import repository.AccountRepository;
import repository.InMemoryAccountRepository;
import domain.Account;
import java.util.List;
import service.TransferService;
import service.AccountService;

@Controller
public class TransferController {
	
	@RequestMapping("/transfer")
	String transfer(ModelMap model) {
		
		AccountRepository repository = new InMemoryAccountRepository();
		AccountService accounts = new AccountService(repository);
		accounts.genAccounts();
		TransferService transfer = new TransferService(accounts.getRepository());

		model.addAttribute("accounts",accounts.listAccounts());

		return "transfer";  

	}

	@RequestMapping("/transfer/concrete")
	String concretetransfer(ModelMap model, @RequestParam String source, @RequestParam String target, @RequestParam Double amount) {
		
		AccountRepository repository = new InMemoryAccountRepository();
		AccountService accounts = new AccountService(repository);
		accounts.genAccounts();
		
		AccountRepository oldrepo = accounts.getRepository();


		TransferService transfer = new TransferService(accounts.getRepository());
		transfer.transfer(source,target,amount);
			
			AccountRepository newrepo = transfer.getRepository();

			model.addAttribute("source",oldrepo.findByNumber(source));
			model.addAttribute("target",oldrepo.findByNumber(target));

			model.addAttribute("newsource",newrepo.findByNumber(source));
			model.addAttribute("newtarget",newrepo.findByNumber(target));

			
		return "concretetransfer";  





	}

}
