package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import repository.AccountRepository;
import repository.InMemoryAccountRepository;
import domain.Account;
import java.util.List;

import service.AccountService;

@Controller
public class AccountController {
	
	@RequestMapping("/accounts")
	String ShowAllAccounts(ModelMap model) {
		
		AccountRepository repository = new InMemoryAccountRepository();
		AccountService accounts = new AccountService(repository);
		accounts.genAccounts();
		model.addAttribute("listAccounts",accounts.listAccounts());
		return "showaccount";  

	}

}
