package repository;

import java.util.List;

import domain.Account;

public interface AccountRepository {

	boolean findAccount(String number);

	Account findByNumber(String number);

	List<Account> findAll();

	Account save(Account account);

	Account remove(Account account);
}
